package br.ucsal.bes20201.ed.Avalia��o01;

public interface Lista<T> {

/**
* Adiciona um elemento no final da lista atual .
*
* @param element elemento a ser adicionado.
*/
void add(T element);

/**
* Retorna o elemento da lista, na posi��o especificada.
* Lembre-se de que o primeiro elemento de uma lista est� no index 0 (zero).
*
* @param index posi��o do elemento a ser retornado
* @return o elemento da posi��o especificada
*/
T get(int index);

/**
* Retorna o tamanho da lista
*
* @return tamanho da lista
*/
int size();

/**
* Adiciona o elemento passado como par�metro numa posi��o espec�fica,
* deslocando os demais elementos para frente na lista. Por exemplo, para lista
* ["antonio", "pedreira", "neiva"] a solicita��o do add(1,"claudio")
* far� com que a lista se torne ["antonio", "claudio", "pedreira", "neiva"].
* Lembre-se de que o primeiro elemento de uma lista est� no index 0 (zero).
*
* @param index �ndice no qual o elemento ser� adicionado
* @param element elemento que ser� adicionado
*/
void add(int index, T element);

}