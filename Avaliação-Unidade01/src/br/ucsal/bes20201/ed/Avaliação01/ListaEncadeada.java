package br.ucsal.bes20201.ed.Avalia��o01;


public class ListaEncadeada<T> implements Lista<T> {

	private No inicio;

	private int fim = 0;

	@Override
	public void add(T element) {
		No novo = new No(element);
		if (inicio == null) {
			inicio = novo;

		} else {

			No aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;

			}
			aux.prox = novo;
		}
		fim++;
	}

	@Override
	public T get(int index) {
		No aux = inicio;
		for (int i = 0; i < index; i++) {
			aux = aux.prox;
		}
		return aux.elemento;
	}

	@Override
	public int size() {
		
		return fim;
	}

	@Override
	public void add(int index, T element) {
		

	}

	private class No {
		private T elemento;
		private No prox;

		private No(T elemento) {
			this.elemento = elemento;
			
		}

	}
}
